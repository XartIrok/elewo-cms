<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012-09-09 Elewo engine.
 */
 
# Load main file to lunch page
include_once('./system/load.data.php');

# Load page params
include_once('./system/load.page.php');

# Load layout of page
include_once('./system/load.style.php');