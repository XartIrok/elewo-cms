<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2013-05-14 Elewo engine.
 * ------------------
 * Language: English
 * Author: Xart
 * ------------------
 */
$lang = array();

$lang['errors']['title'] = 'Information wrong on page';
$lang['errors']['empty'] = 'Lack of translation';
# list of number errors
$lang['errors']['404'] = 'Page not found';