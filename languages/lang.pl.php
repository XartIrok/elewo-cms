<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2013-05-14 Elewo engine.
 * ------------------
 * Language: Polski
 * Author: Xart
 * ------------------
 */
$lang = array();

$lang['errors']['title'] = 'Informacja o błędzie strony';
$lang['errors']['empty'] = 'Brak tłumaczenia';
# lista numerow bledow
$lang['errors']['404'] = 'Nie znalziono strony';