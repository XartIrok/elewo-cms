<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-11-21
 * @DateUpdate: 2013-08-08
 */

if (!$logged)
{
	$succes = null;

	$user = stripcslashes(trim($_REQUEST['reqLogin']));
	$mail = stripcslashes(trim($_REQUEST['reqEmail']));
	$pass1 = stripcslashes(trim($_REQUEST['reqPass1']));
	$pass2 = stripcslashes(trim($_REQUEST['reqPass2']));

	if ($_POST)
	{
		if (empty($user))
			$anyErrors[] = 'Nie wprowadzono nazwy użytkownika';
		if (empty($mail))
			$anyErrors[] = 'Nie wprowadzono adresu mailowego';
		else
			if (!checkEmail($mail))
				$anyErrors[] = 'Wprowadzony adres mailowy jest nie poprawny';
		if (empty($pass1))
			$anyErrors[] = 'Nie wprowadzono żadnego hasło';
		else
			if ($pass1 != $pass2)
				$anyErrors[] = 'Wprowadzone hasła nie są identyczne';

		if (empty($anyErrors))
		{
			if ($config['site']['generatSalt'])
				$salt = hash($config['site']['hashAlgorithm'], generateSalt());
			else
				$salt = '';
			$pass = hashPassword($config['site']['hashAlgorithm'], $pass1, $salt);

			$user = strtolower($user);
			$mail = strtolower($mail);
			$time = time();
			$ipUser = ip2long($_SERVER['REMOTE_ADDR']);

			$stmt = $SQL->prepare("INSERT INTO accounts (name, password, salt, email) VALUES (:name, :pass, :salt, :mail);");
			$stmt->bindParam(':name', $user, PDO::PARAM_STR);
			$stmt->bindParam(':pass', $pass, PDO::PARAM_STR);
			$stmt->bindParam(':salt', $salt, PDO::PARAM_STR);
			$stmt->bindParam(':mail', $mail, PDO::PARAM_STR);
			$stmt->execute();
			$stmt->closeCursor();

			$stmt = $SQL->prepare("INSERT INTO accounts_info (id_account, username, time_create, ip_create, country) SELECT `id`, :name, :time, :ip, :country FROM accounts ORDER BY `id` DESC LIMIT 1;");
			$stmt->bindParam(':name', $user, PDO::PARAM_STR);
			$stmt->bindParam(':time', $time, PDO::PARAM_INT);
			$stmt->bindParam(':ip', $ipUser, PDO::PARAM_INT);
			$stmt->bindParam(':country', $_REQUEST['reqCountry'], PDO::PARAM_STR);
			$stmt->execute();
			$stmt->closeCursor();

			$content .= '<div class="alert alert-success">Twoje konto zostało poprawnie założone, możesz teraz zalogować się na nie.</div>';
		}
		else
		{
			foreach($anyErrors as $anyError) 
				$content .= '<div class="alert alert-error">'.$anyError.'</div>';
		}
	}
	if (!$_POST || !$succes)
	{
		$content .= '<form method="post">
			<table width="100%">
				<tr>
					<td>Login/Nick:</td>
					<td><input type="text" name="reqLogin"></td>
				</tr>
				<tr>
					<td>Adres Email:</td>
					<td><input type="text" name="reqEmail"></td>
				</tr>
				<tr>
					<td>Hasło:</td>
					<td><input type="password" name="reqPass1"></td>
				</tr>
				<tr>
					<td>Powtórz hasło:</td>
					<td><input type="password" name="reqPass2"></td>
				</tr>
			</table>
			<input type="hidden" name="reqCountry" value="'.geoIpCountryCode($_SERVER['REMOTE_ADDR']).'" />
			<button class="btn" type="sumbit" style="margin-top: 5px;">Zarejestruj</a>
		</form>';
	}
}
else
	$content .= '<div class="alert alert-error">Jesteś już zalogowany i nie możesz się kolejny raz zarejestrować</div>';