<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012-11-21 Footer35mm.
 */

if (!$logged)
{
	$succes = null;

	if ($_POST)
	{
		$user = stripcslashes(trim($_REQUEST['reqLogin']));
		$pass = stripcslashes(trim($_REQUEST['reqPass']));

		$stmt = $SQL->prepare("SELECT name, salt FROM accounts WHERE name=:login OR email=:login;");
		$stmt->bindParam(':login', strtolower($user), PDO::PARAM_STR);
		$stmt->execute();
		if ($login = $stmt->fetch(PDO::FETCH_ASSOC))
		{
			if (empty($pass))
				$anyErrors[] = 'Nie wprowadzono żadnego hasła';
			else
			{
				$pass = hashPassword($config['site']['hashAlgorithm'], $pass, $login['salt']);

				$result = $SQL->prepare("SELECT id FROM accounts WHERE password=:pass;");
				$result->bindParam(':pass', $pass, PDO::PARAM_STR);
				$result->execute();
				if ($info = $result->fetch(PDO::FETCH_ASSOC))
				{
					$stmtu = $SQL->prepare("UPDATE accounts_info SET time_lastlogin=:time WHERE id_account=:id;");
					$stmtu->bindParam(':time', time(), PDO::PARAM_INT);
					$stmtu->bindParam(':id', $info['id'], PDO::PARAM_INT);
					$stmtu->execute();
					$stmtu->closeCursor();

					$_SESSION['login'] = TRUE;
	                $_SESSION['id_account'] = $info['id'];

					redirect(array(subtopic => 'account'));
				}
				else
				{
					$anyErrors[] = 'Nie poprawny login lub email';
					$anyErrors[] = 'Nie poprawne hasło';
				}
				$result->closeCursor();
			}
		}
		else
		{
			if (empty($user))
				$anyErrors[] = 'Nie wprowadzono żadnego loginu lub adres email';
			else
				$anyErrors[] = 'Nie poprawny login lub email';
			if (empty($pass))
				$anyErrors[] = 'Nie wprowadzono żadnego hasła';
		}
		$stmt->closeCursor();

		foreach($anyErrors as $anyError) 
			$content .= '<div class="alert alert-error">'.$anyError.'</div>';
	}
	if (!$_POST || !$succes)
	{
		$form .= '<form method="post">
			<table width="100%">
				<tr>
					<td>Login/Email:</td>
					<td><input type="text" name="reqLogin"></td>
				</tr>
				<tr>
					<td>Hasło:</td>
					<td><input type="password" name="reqPass"></td>
				</tr>
			</table>
			<button class="btn" type="sumbit" style="margin-top: 5px;">Zarejestruj</a>
		</form>';

		$content .= '<div class="row-fluid">
			<div class="span8">
				'.$form.'
			</div>
			<div class="span4">
			</div>
		</div>';
	}
}
else
	$content .= '<div class="alert alert-error">Jesteś już zalogowany</div>';