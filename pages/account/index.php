<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2013-07-13 lista.
 */
 
if ($page == '')
	$page = 'index';

if (file_exists(__DIR__.'/modules/'.$page.'.php'))
	include_once (__DIR__.'/modules/'.$page.'.php');
else
	redirect(array(subtopic => 'errors', param => 404));
