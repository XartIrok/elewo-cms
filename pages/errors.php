<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2013-05-22 Elewo engine.
 */

$disableRenderLayout = true;

$errorMessage = empty($lang['errors'][$param[0]]) ? $lang['errors']['empty'] : $lang['errors'][$param[0]];

echo '<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>'.TITLE.'</title>
		<style>
			body {
				background-image:url("./images/site/death.png");
				background-repeat:no-repeat;
				background-color:#30472b;
				background-attachment:fixed;
				background-position:98% 98%;
			}
			.box {
				color: #fff;
				width: 500px;
				margin: 10% auto 0px;
				font-size: 24px;
				text-shadow: 1px 1px 4px #000;
			}
		</style>
	</head>

	<body>
		<div class="box">
			<table width="100%">
				<tr>
					<td>'.getLanguageText($lang['errors']['title']).'</td>
					<td align="right"><img src="./images/site/elewo.new.png" style="height:50px"></td>
				</tr>
				<tr>
					<td colspan="2">'.getLanguageText($errorMessage).'</td>
				</tr>
			</table>
		</div>
	</body>
</html>';