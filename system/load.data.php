<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-09-09
 * @DateUpdate: 2013-08-08
 */
 
# Start of session
session_start();

# time start in load scripts
$time_start = microtime(true);

# Function to load config file
$dir = "./system/config/";
if ($handle = opendir($dir))
{
	while (false !== ($file = readdir($handle))) {
		if($file != "." && $file != "..")
			include_once($dir.$file);
	}
	closedir($handle);
}

# rewrite uri system
if($config['site']['useRewrite'])
{
	$_uri = explode('/', trim(substr($_SERVER['REQUEST_URI'], strlen(dirname($_SERVER['SCRIPT_NAME']))), '/'));
	foreach($_uri as $key => $value)
		$_uri[$key] = urldecode($value);
	$subtopic = $_uri[0];
	$page = $_uri[1];
	$action = $_uri[2];
	$param = preg_split('/\s*,\s*/', stripcslashes(strtolower(trim($_uri[2]))), -1, PREG_SPLIT_NO_EMPTY);  
}
else
{
	$subtopic = empty($_REQUEST['subtopic']) ? $config['site']['startPage'] : $_REQUEST['subtopic'];
	$page = empty($_REQUEST['page']) ? '' : $_REQUEST['page'];
	$action = empty($_REQUEST['action']) ? '' : $_REQUEST['action'];
	$paramString = empty($_REQUEST['param']) ? '' : $_REQUEST['param'];
	$param = preg_split('/\s*,\s*/', stripcslashes(strtolower(trim($paramString))), -1, PREG_SPLIT_NO_EMPTY);
}

# Function to load class from file
function classLoadByName($nameClass)
{
	if (!class_exists($nameClass))
		if (file_exists("./system/classes/class.".strtolower($nameClass).".php"))
			include_once("./system/classes/class.".strtolower($nameClass).".php");
		else
			echo 'Cannot load class <b>'.$nameClass.'</b>, file <b>./system/classes/class.'.strtolower($className).'.php</b> doesn\'t exist';
}

# Function for debugging website
error_reporting(E_ALL ^ E_NOTICE); 

# Auto Load all class
spl_autoload_register('classLoadByName');

# Load more function
include_once('./system/load.function.php');

# Load config to connect database
include_once('./system/load.database.php');

# checkout a information from info.xml
$project = simplexml_load_file("./info.xml");
if (empty($config['site']['mainTitle']))
	$config['site']['mainTitle'] = $project->name;
$projectVersion = $project->version;

# checkout a information from repository
$repositoryVersion = $projectVersion;
$urlChekoutInfo = ''; // ToDo: add url from checkout info
if (!empty($urlChekoutInfo))
{
	$context = stream_context_create(array('http' => array('header' => 'Accept: application/xml')));
	$repository = simplexml_load_string(file_get_contents($urlChekoutInfo, false, $context));
	$repositoryVersion = $repository->version;
}

# checkout version scripts
if ($repositoryVersion > $projectVersion)
{
	echo 'Avaible a new version of the scripts';
}

# stable session login
if(isset($_SESSION['login']))
{
	$logged = TRUE;
	$time = time();
	$ipUser = ip2long($_SERVER['REMOTE_ADDR']);
	$idUser = $_SESSION['id_account'];

	$stmt = $SQL->prepare("UPDATE account_info SET time_lastactive=:time, ip_lastactive=:ip WHERE id_account=:id;");
	$stmt->bindParam(':time', $time, PDO::PARAM_INT);
	$stmt->bindParam(':ip', $ipUser, PDO::PARAM_INT);
	$stmt->bindParam(':id', $idUser, PDO::PARAM_INT);
	$stmt->execute();
	$stmt->closeCursor();

	$stmt = $SQL->prepare("SELECT * FROM account_info WHERE id_account=:id;");
	$stmt->bindParam(':id', $idUser, PDO::PARAM_INT);
	$stmt->execute();
	$user = $stmt->fetch(PDO::FETCH_ASSOC);
	$stmt->closeCursor();

	$idGroup = $user['id_group'];
}
else
{
	$logged = false;
}

# set template
Layout::$template = $config['site']['layout'];

# Stable varible
define('PATH_SCRIPT', './system/scripts', true);