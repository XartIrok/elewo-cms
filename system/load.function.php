<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-09-09
 * @DateUpdate: 2013-08-08
 */
 
function validatePassword($algo, $password, $salt, $confrimPassword)
{
	return hashPassword($algo, $password, $salt)===$confrimPassword;
}

function hashPassword($algo, $password, $salt)
{
	if ($algo == 'plain')
		return $salt.$password;
	else
		return hash($algo, $salt.$password);
}

function generateSalt()
{
	return uniqid('', true);
}

function getLanguageText($string, $arg1=null, $arg2=null, $arg3=null, $arg4=null, $arg5=null, $arg6=null)
{
	if (!is_null($arg1)) {
		$string = sprintf($string, $arg1, $arg2, $arg3, $arg4, $arg5, $arg6);
	}
	return $string;
}

function checkEmail($email) {
	$spam = array('sina', 'spambox', 'mailinator');
	$mail = explode("@", $email);
	$host = explode(".", $mail[1]);
	if (in_array($host[0], $spam))
		return false;
	if (!preg_match("/^[a-zA-Z0-9\.\-_]+\@[a-zA-Z0-9\.\-_]+\.[a-z]{2,4}$/D", $email))
		return false;
	return true;
}

function geoIpCountryCode($ip)
{
	include("./system/scripts/geoip/geoip.php");

	$gi = geoip_open("./system/scripts/geoip/GeoIP.dat", GEOIP_STANDARD);
	$geo = geoip_country_code_by_addr($gi, $ip);
	geoip_close($gi);
	
	return $geo;
}

define('URI', $config['site']['useRewrite']);

function createUrl(array $data, $uri = URI)
{
	$_url = '';
	if(!file_exists('./.htaccess'))
		$_url .= $_SERVER["SCRIPT_NAME"];
	if($uri == true)
	{
		$_url .= '/';
		foreach($data as $url)
			$_url .= $url.'/';
	}
	else
	{
		if (!empty($data) || !file_exists('./.htaccess'))
			$_url .= '?';
		foreach($data as $typ => $url)
			$_url .= $typ.'='.$url.'&';
	}
	return substr($_url, 0, -1);
}

function redirect($param = '', $refresh = 0)
{
	if (!$refresh)
		return header('Location: http://'.$_SERVER['HTTP_HOST'].createUrl($param));
	else
		return header('refresh:'.$refresh.';url='.$param);
}

function shortText($text, $chars_limit)
{
	if (strlen($text) > $chars_limit)
		return substr($text, 0, strrpos(substr($text, 0, $chars_limit), " ")).'...';
	else
		return $text;
}

function countSize ($number)
{
	if (strlen($number) > 9)
		$val = round(((($number / 1024) / 1024) / 1024), 2).' GB';
	else if (strlen($number) > 6)
		$val = round((($number / 1024) / 1024), 2).' MB';
	else if (strlen($number) > 3)
		$val = round(($number / 1024), 2).' KB';
	else
		$val = round(($number), 2).' B';
	return $val;
}

function showTime($param, $separator = ' ') 
{
	$today = strtotime(date('M j, Y'));
	$reldays = ($param - $today)/86400;
	
	if ($reldays >= 0 && $reldays < 1)
		return 'Dzisiaj'.$separator.date('H:i', $param);
	else if ($reldays >= -1 && $reldays < 0) 
		return 'Wczoraj'.$separator.date('H:i', $param);
	else
		return date('d-m-Y', $param).$separator.date('H:i', $param);
}

function clearHttp($param, $https=false)
{
	if (substr($param, 0, 5) == 'https')
		$https = true;
	if ($https)
		$param = substr($param, 8);
	else
		$param = substr($param, 7);
	if (substr($param, -1) == '/')
		$param = substr($param, 0, -1);
	return $param;
}

function createFolderFile($name, $chmod = 0777)
{
	if (!is_dir($name))
		mkdir($name, $chmod, true);
	return $name;
}

function reArrayFiles(&$file_post) 
{
	$file_ary = array();
	$file_count = count($file_post['name']);
	$file_keys = array_keys($file_post);

	for ($i=0; $i<$file_count; $i++) {
		foreach ($file_keys as $key) {
			$file_ary[$i][$key] = $file_post[$key][$i];
		}
	}

	return $file_ary;
}

function moveUploadFile(array $data, $katalog, $limitSize = 5000000000)
{
	if ($data[1] < $limitSize)
	{
		if ($data[3] > 0)
			return 'Return Code: '.$data[3].'<br>';
		else
		{
			if (!is_dir('./upload/'.$katalog.'/'))
				mkdir('./upload/'.$katalog.'/', 0777, true);
			if (file_exists('./upload/'.$katalog.'/'.$data[0]))
				return $data[0].' already exists. ';
			else
			{
				move_uploaded_file($data[2], './upload/'.$katalog.'/'.$data[0]);
				return 'Stored in: ' . './upload/'.$katalog.'/'.$data[0];
			}
		}
	}
	else
		return 'Invalid file';
}