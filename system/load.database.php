<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-09-09
 * @DateUpdate: 2013-08-07
 */

if (strtolower($config['database']['type']) == 'mysql')
{
	Website::setDBDriver(Database::DB_MYSQL);
	Website::getDBHandle()->setHost($config['database']['host']);
	Website::getDBHandle()->setPort($config['database']['port']);
	Website::getDBHandle()->setName($config['database']['name']);
	Website::getDBHandle()->setUser($config['database']['user']);
	Website::getDBHandle()->setPass($config['database']['pass']);
	Website::getDBHandle()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
}
elseif (strtolower($config['database']['type']) == 'pgsql')
{
	Website::setDBDriver(Database::DB_PGSQL);
	Website::getDBHandle()->setHost($config['database']['host']);
	Website::getDBHandle()->setPort($config['database']['port']);
	Website::getDBHandle()->setName($config['database']['name']);
	Website::getDBHandle()->setUser($config['database']['user']);
	Website::getDBHandle()->setPass($config['database']['pass']);
}
elseif (strtolower($config['database']['type']) == 'sqlite')
{
	Website::setDBDriver(Database::DB_SQLITE);
	Website::getDBHandle()->setFile($config['database']['file']);
}
else
{
	header('Location: http://'.$_SERVER["HTTP_HOST"].'/pages/errors/connectbase.php');
	exit;
}
$SQL = Website::getDBHandle();