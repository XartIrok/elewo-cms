<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-09-09
 * @DateUpdate: 2013-08-09
 */

# the main variables of layout
define('LAYOUT', $config['site']['layout']);
define('CHARSET', 'UTF-8');
define('PATH_LAYOUT', './styles/'.LAYOUT.'/');

# count time in second load script
$time = round(microtime(true) - $time_start, 4);

# varbiles to layout.tpl
$data = array(
	'title' => TITLE, 
	'content' => $main, 
	'charset' => CHARSET, 
	'path' => PATH_LAYOUT, 
	'script' => $script, 
	'styles' => $styles,
	'panelLogin' => $login,
	'loadTime' => $time
);

# display layout.tpl
if (!$disableRenderLayout)
{
	$themes = new Layout(LAYOUT);
	echo $themes->render($data);
}