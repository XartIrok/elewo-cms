<?php

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2013-08-07
 * @DateUpdate: 2013-08-07
 */

class Website 
{
	public static $SQL;

	public static function setDBDriver($value)
	{
		self::$SQL = null;
		switch($value)
		{
			case Database::DB_MYSQL:
				self::$SQL = new ConnectDatabase(Database::DB_MYSQL);
				break;
			case Database::DB_PGSQL:
				self::$SQL = new ConnectDatabase(Database::DB_PGSQL);
				break;
			case Database::DB_SQLITE:
				self::$SQL = new ConnectDatabase(Database::DB_SQLITE);
				break;
		}
	}

	public static function getDBHandle()
	{
		if(isset(self::$SQL))
			return self::$SQL;
		else
			return false;
	}
}
?>