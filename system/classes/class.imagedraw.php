<?php
define('MAX_SIZE_EXCEDED', 101);
define('UPLOAD_FAILED', 102);
define('NO_UPLOAD', 103);
define('NOT_IMAGE', 104);
define('INVALID_IMAGE', 105);
define('NONEXISTANT_PATH', 106);

define('FILE_EXSITE', 111);

class ImageDraw
{
	var $tmp_name;
	var $name;
	var $size;
	var $type;
	var $error;
	var $width_orig;
	var $height_orig;
	var $num_type;
	var $errorCode = 0;
	var $allow_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG);
	var $imagePath;

	function __construct($fileArray)
	{
		foreach($fileArray as $key => $value)
			$this->$key = $value;
		if($this->error > 0)
		{
			switch ($this->error)
			{
				case 1: $this->errorCode = MAX_SIZE_EXCEDED; break;
				case 2: $this->errorCode = MAX_SIZE_EXCEDED; break;
				case 3: $this->errorCode = UPLOAD_FAILED; break;
				case 4: $this->errorCode = NO_UPLOAD; break;
			}
		}
		if($this->errorCode == 0)
			$this->secure();
	}

	function secure()
	{
		@list($this->width_orig, $this->height_orig, $this->num_type) = getimagesize($this->tmp_name);
		
		if(filesize($this->tmp_name) > 1024*1024*1024*5)
		{
			$this->errorCode = MAX_SIZE_EXCEDED;
			return false;
		}
		if (!$this->num_type)
		{
			$this->errorCode = NOT_IMAGE;
			return false;
		}
		if(!in_array($this->num_type, $this->allow_types))
		{
			$this->errorCode = INVALID_IMAGE;
			return false;
		}
	}

	function getError()
	{
		return $this->errorCode;
	}

	function getType() 
	{
		switch($this->num_type)
		{
			case IMAGETYPE_GIF: $ext = '.gif'; break;
			case IMAGETYPE_JPEG: $ext = '.jpg'; break;
			case IMAGETYPE_PNG: $ext = '.png'; break;
		}
		return $ext;
	}

	function uploadOrginal($folder, $name)
	{
		switch($this->num_type)
		{
			case IMAGETYPE_GIF: $ext = '.gif'; break;
			case IMAGETYPE_JPEG: $ext = '.jpg'; break;
			case IMAGETYPE_PNG: $ext = '.png'; break;
		}
		$this->imagePath = $folder.$name.$ext;
		return move_uploaded_file($this->tmp_name, $folder.$name.$ext);
	}

	function upload($folder, $name, $width, $height, $scaleUp = false)
	{
		if($this->errorCode > 0)
			return false;
		if((!$scaleUp && ($width > $this->width_orig && $height > $this->height_orig)) || ($width === "0" && $height === "0"))
		{
			$width = $this->width_orig;
			$height = $this->height_orig;
		}
		elseif ($scaleUp == true && $width < $this->width_orig && $height < $this->height_orig)
		{
			$width = $width; $height = $height;
		}
		else
		{
			if(($this->height_orig - $height) <= ($this->width_orig - $width))
				$height = ($width / $this->width_orig) * $this->height_orig;
			else
				$width = ($height / $this->height_orig) * $this->width_orig;
		}

		switch($this->num_type)
		{
			case IMAGETYPE_GIF: $image_o = imagecreatefromgif($this->imagePath); $ext = '.gif'; break;
			case IMAGETYPE_JPEG: $image_o = imagecreatefromjpeg($this->imagePath); $ext = '.jpg'; break;
			case IMAGETYPE_PNG: $image_o = imagecreatefrompng($this->imagePath); $ext = '.png'; break;
		}
		$filepath = $folder.(substr($folder,-1) != '/' ? '/' : '');
		if(is_dir($filepath))
			$filepath .= $name.$ext;
		else
		{
			$this->errorCode = NONEXISTANT_PATH;
			imagedestroy($image_o);
			return false;
		}
		$image_r = imagecreatetruecolor($width, $height);
		imagecopyresampled($image_r, $image_o, 0, 0, 0, 0, $width, $height, $this->width_orig, $this->height_orig);
		
		switch($this->num_type)
		{
			case IMAGETYPE_GIF: imagegif($image_r, $filepath); break;
			case IMAGETYPE_JPEG: imagejpeg($image_r, $filepath); break;
			case IMAGETYPE_PNG: imagepng($image_r, $filepath); break;
		}

		imagedestroy($image_o);
		imagedestroy($image_r);

		return true;
	}
}