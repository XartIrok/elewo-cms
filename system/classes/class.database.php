<?php

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2013-08-07
 * @DateUpdate: 2013-08-07
 */

class Database extends PDO
{
	const DB_ODBC 	= 1;
	const DB_MYSQL 	= 2;
	const DB_PGSQL 	= 3;
	const DB_SQLITE = 4;

	private $connected = false;
	private $dbDriver;

	private $dbHost = 'localhost';
	private $dbPort;
	private $dbName;
	private $dbUser;
	private $dbPass;
	private $dbFile;

	public function setDriver($dbDriver)
	{
		$this->dbDriver = $dbDriver;
	}

	public function getDriver()
	{
		return $this->dbDriver;
	}

	public function isConnected()
	{
		return $this->connected;
	}

	public function setConnected($value)
	{
		$this->connected = $value;
	}

	// function to set information to connect database

	public function setHost($dbHost)
	{
		$this->dbHost = $dbHost;
	}

	public function setPort($dbPort)
	{
		$this->dbPort = $dbPort;
	}

	public function setName($dbName)
	{
		$this->dbName = $dbName;
	}

	public function setUser($dbUser)
	{
		$this->dbUser = $dbUser;
	}

	public function setPass($dbPass)
	{
		$this->dbPass = $dbPass;
	}
	
	public function setFile($dbFile)
	{
		$this->dbFile = $dbFile;
	}

	// function to get information to connect database

	public function getHost()
	{
		return $this->dbHost;
	}

	public function getPort()
	{
		return $this->dbPort;
	}

	public function getName()
	{
		return $this->dbName;
	}

	public function getUser()
	{
		return $this->dbUser;
	}

	public function getPass()
	{
		return $this->dbPass;
	}
	
	public function getFile()
	{
		return $this->dbFile;
	}

	// function from class PDO

	public function beginTransaction()
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::beginTransaction();
	}

	public function commit()
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::commit();
	}

	public function errorCode()
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::errorCode();
	}

	public function errorInfo()
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::errorInfo();
	}

	public function exec($statement)
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::exec($statement);
	}

	public function getAttribute($attribute)
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::getAttribute($attribute);
	}

	public static function getAvailableDrivers()
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::getAvailableDrivers();
	}

	public function inTransaction()
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::inTransaction();
	}

	public function lastInsertId($statement = null)
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::lastInsertId($statement);
	}

	public function prepare($statement, $options = array())
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::prepare($statement, $options);
	}

	public function query($statement)
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::query($statement);
	}

	public function quote($statement, $param = PDO::PARAM_STR)
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::quote($statement, $param);
	}

	public function rollBack()
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::rollBack();
	}

	public function setAttribute($attribute, $value)
	{
		if($this->isConnected() || $this->databaseConnect())
			return parent::setAttribute($attribute, $value);
	}
}
?>