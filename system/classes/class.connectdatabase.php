<?php

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2013-08-07
 * @DateUpdate: 2013-08-07
 */

class ConnectDatabase extends Database
{
	public $mark;
	
	public function __construct($value)
	{
		$this->setDriver($value);
	}

	public function databaseConnect()
	{
		try 
		{
			switch($this->getDriver())
			{
				case self::DB_MYSQL:
					parent::__construct('mysql:host='.$this->getHost().';port='.$this->getPort().';dbname='.$this->getName(), $this->getUser(), $this->getPass());
					$this->mark = '`';
					break;
				case self::DB_PGSQL:
					parent::__construct('pgsql:host='.$this->getHost().';port='.$this->getPort().';dbname='.$this->getName(), $this->getUser(), $this->getPass());
					$this->mark = '"';
					break;
				case self::DB_SQLITE:
					parent::__construct('sqlite:'.$this->getFile());
					$this->mark = '"';
					break;
			}
			$this->setConnected(true);
			return true;
		}
		catch (PDOException $error)
		{
			return false;
		}
	}

	public function fieldName($name)
	{
		if(strspn($name, "1234567890qwertyuiopasdfghjklzxcvbnm_") != strlen($name))
			return false;
		return $this->mark.$name.$this->mark;
	}

	public function tableName($name)
	{
		if(strspn($name, "1234567890qwertyuiopasdfghjklzxcvbnm_") != strlen($name))
			return false;
		return $this->mark.$name.$this->mark;
	}
}
?>