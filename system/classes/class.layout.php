<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-09-29
 * @DateUpdate: 2013-08-09
 */
 
class Layout
{
	public $render;
	public $layout;
	public static $template;
	
	public function __construct($layout) {
		$this->layout = $layout;
	}

	public static function loadView($file, $modul, $options = array(), $ext = 'html')
	{
		return strtr(file_get_contents('./pages/'.$file.'/view/'.self::$template.'/'.$modul.'.'.$ext), $options);
	}

	public static function loadTemplate($modul, $options = array(), $ext = 'html')
	{
		return strtr(file_get_contents('./styles/'.self::$template.'/template/'.$modul.'.'.$ext), $options);
	}

	public function render($data)
	{
		$this->render = file_get_contents('./styles/'.$this->layout.'/layout.tpl');
		foreach($data as $c => $content)
		{
			$this->render = str_replace('{{'.$c.'}}', $content, $this->render);
		}
		$this->render = preg_replace('({{(.*?)}})', '', $this->render);
		return $this->render;
	}
	
	public function __destruct()
	{
		unset($this->render);
	}
}