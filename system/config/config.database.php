<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012-09-09 Elewo engine.
 */
 
# choise of type connet database on site can use now: mysql, sqlite
$config['database']['type'] = 'mysql';

# load database whe use sqlite
$config['database']['file'] = '';

# connet into database type: mysql, pgsql, sqlite
# port: mysql 3306, pgsql 5432
$config['database']['host'] = 'localhost';
$config['database']['port'] = 3306;

# load database the name
$config['database']['name'] = '';

# login into database
$config['database']['user'] = 'root';
$config['database']['pass'] = '';

$config['database']['prefix'] = '';