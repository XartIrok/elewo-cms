<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-09-09
 * @DateUpdate: 2013-08-08
 */
 
# set the first load page if file dosen't exsite show error 404
$config['site']['startPage'] = 'main';

# default langauge on site
$config['site']['defaultLangauge'] = 'en';

# algorithm use to hash a password account
$config['site']['hashAlgorithm'] = 'sha1';

# generation salt to better safe password
$config['site']['generatSalt'] = true;

# title on page if empty show name a project 
$config['site']['mainTitle'] = '';

$config['site']['useRewrite'] = false;

$config['site']['layout'] = 'default';

$config['site']['debug'] = false;