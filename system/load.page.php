<?PHP

/**
 * @Author: Xart.
 * @License: FreeBSD.
 * @Copyright: 2012 Elewo engine.
 * @DateCreate: 2012-09-09
 * @DateUpdate: 2013-08-07
 */

# if rendering is enabled
$disableRenderLayout = false;

# remover magic quotes
if (get_magic_quotes_gpc() && function_exists('get_magic_quotes_gpc')) {
	$process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
	while (list($key, $val) = each($process)) {
		foreach ($val as $k => $v) {
			unset($process[$key][$k]);
			if (is_array($v)) {
				$process[$key][stripslashes($k)] = $v;
				$process[] = &$process[$key][stripslashes($k)];
			} else {
				$process[$key][stripslashes($k)] = stripslashes($v);
			}
		}
	}
	unset($process);
}

# multi language on site
header('Cache-control: private');

if (isset($_REQUEST['lang']))
{
	$lang = $_REQUEST['lang'];
	$_SESSION['lang'] = $lang;
	setcookie('lang', $lang, time() + (3600 * 24 * 30));
}
elseif (isset($_SESSION['lang']))
	$lang = $_SESSION['lang'];
elseif (isset($_COOKIE['lang']))
	$lang = $_COOKIE['lang'];
else
	$lang = $config['site']['defaultLangauge'];

if (is_file('./languages/lang.'.$lang.'.php'))
	include_once ('./languages/lang.'.$lang.'.php');

# Constans define
define('TITLE', ucwords($subtopic)." - ".$config['site']['mainTitle']);
define('PATH_PAGES', "./pages");

# Loading a page or error page
if(is_file(PATH_PAGES.'/'.$subtopic.'.php'))
	include_once (PATH_PAGES.'/'.$subtopic.'.php');
elseif (is_file(PATH_PAGES.'/'.$subtopic.'/index.php'))
	include_once (PATH_PAGES.'/'.$subtopic.'/index.php');
else
	redirect(array(subtopic => 'errors', param => 404));