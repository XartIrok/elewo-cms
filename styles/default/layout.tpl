<!DOCTYPE html>
<html>
	<head>
		<title>{{title}}</title>
		
		<meta http-equiv="Content-Type" content="text/html; charset={{charset}}" />
		{{styles}}
		
		<link rel="stylesheet" href="{{path}}css/basic.css" type="text/css" />
		{{script}}
	</head>
	<body>
		{{content}}
	</body>
</html>