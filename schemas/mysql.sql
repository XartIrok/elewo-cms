CREATE TABLE IF NOT EXISTS `accounts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NULL,
  `password` VARCHAR(255) NULL,
  `salt` VARCHAR(255) NULL,
  `email` VARCHAR(255) NULL,
  PRIMARY KEY (`id`), UNIQUE (`name`)
) ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `accounts_info` (
  `id_account` INT NULL,
  `username` VARCHAR(255),
  `time_create` INT NULL,
  `time_lastlogin` INT NULL,
  `time_lastactive` INT NULL,
  `ip_create` INT NULL,
  `ip_lastactive` INT NULL,
  `id_group` INT DEFAULT '1',
  `country` VARCHAR(2) NULL,
  `code_permission` TEXT NULL,
  KEY (`id_account`),
  FOREIGN KEY (`id_account`) REFERENCES `accounts`(`id`) ON DELETE CASCADE 
) ENGINE = InnoDB;